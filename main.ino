int dot = 100;
int dash = dot * 3;
    
void flashLED(int delayTime) {
    digitalWrite(D7, HIGH);
    delay(delayTime);
    digitalWrite(D7, LOW);
    delay(dot);
}

void setup() {
    pinMode(D7, OUTPUT);
}

void loop() {
    //K
    flashLED(dash);
    flashLED(dot);
    flashLED(dash);
    //O
    flashLED(dash);
    flashLED(dash);
    flashLED(dash);
    //L
    flashLED(dot);
    flashLED(dash);
    flashLED(dot);
    flashLED(dot);
    //A
    flashLED(dot);
    flashLED(dash);
    //D
    flashLED(dash);
    flashLED(dot);
    flashLED(dot);
    //E
    flashLED(dot);
}